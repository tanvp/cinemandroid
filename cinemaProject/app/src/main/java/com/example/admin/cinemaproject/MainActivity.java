package com.example.admin.cinemaproject;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public  String result="";
    public String localHost=" http://35126195.ngrok.io";
    ArrayList <String>arraySpinner;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Intent i = new Intent(this, BookTicket.class);
        //startActivity(i);
        userLocalStore= new UserLocalStore(getBaseContext());
        setNavigationViewListener();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        HomeFragment homeFragment = new HomeFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.mainLayout, homeFragment).commit();

        setupMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home: {
                HomeFragment homeFragment = new HomeFragment();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, homeFragment).addToBackStack("tag").commit();
                break;
            }
            case R.id.nav_login: {
                LoginFragment loginFragment = new LoginFragment();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, loginFragment).addToBackStack("tag").commit();
                break;
            }
            case R.id.nav_movies: {
                FmBookTicket fmBookTicket = new FmBookTicket();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, fmBookTicket).addToBackStack("tag").commit();
                break;
            }

            case R.id.nav_logout: {
                //Tân: set lại trạng thái login
                clearDataLogout();
                setupMenu();
                HomeFragment homeFragment = new HomeFragment();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, homeFragment).addToBackStack("tag").commit();
                break;
            }
            case R.id.nav_history: {
                //Tân: set lại trạng thái login
                FmTransactionHistory fmTransactionHistory = new FmTransactionHistory();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, fmTransactionHistory).addToBackStack("tag").commit();
                break;
            }
        }
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
    }
    public void clearDataLogout(){
        userLocalStore.clearUserData();
        userLocalStore.setUserLoggedIn(false);
    }

    public void setupMenu() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Tân: if ĐÃ đăng nhập

        if (userLocalStore.getUserLoggedIn()) {
            //Show Account và Logout, hide Login
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
            navigationView.getMenu().setGroupVisible(R.id.group_account, true);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
        }
        else {
            //Hide Account và Logout, show Login
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
            navigationView.getMenu().setGroupVisible(R.id.group_account, false);
            navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
        }
    }

    private void setNavigationViewListener() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

}



