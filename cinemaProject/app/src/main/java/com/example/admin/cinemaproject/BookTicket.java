package com.example.admin.cinemaproject;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

public class BookTicket extends AppCompatActivity {
    ArrayList <String>arraySpinner;
    HashMap<Integer,Integer> spinnerMapMovie = new HashMap<Integer, Integer>();
    HashMap<Integer,Integer> spinnerMapFormat = new HashMap<Integer, Integer>();
    public  String result="";
    public String localHost=" http://35126195.ngrok.io";
    public Collection<ShowModel> listShow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_ticket);
    }



    public void getMovies2(){
        final String url =localHost+"/api/Movies/4";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject parentObject = new JSONObject(response);
                            String notes = parentObject.getString("movieJson");
                            Type collectionType = new TypeToken<Collection<MovieModel>>(){}.getType();
                            Gson gson = new Gson();
                            Collection<MovieModel> list = gson.fromJson(notes, collectionType);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(stringRequest);
    }

    public void bookTicketClick(View v){
        try{
            Intent i = new Intent(this, BookSeat.class);
            Bundle bundle = new Bundle();
            Spinner spMovie= (Spinner) findViewById(R.id.spMovie);
            Spinner spFormat= (Spinner) findViewById(R.id.spFormat);
            Spinner spStartTime = (Spinner) findViewById(R.id.spTime);
            Spinner spDate = (Spinner) findViewById(R.id.spDate);
            int idMovie = spinnerMapMovie.get(spMovie.getSelectedItemPosition());
            int formatId = spinnerMapFormat.get(spFormat.getSelectedItemPosition());
            String startTime = spStartTime.getSelectedItem().toString();
            String date = spDate.getSelectedItem().toString();
            for (ShowModel show: listShow ) {
                if(show.getMovieID()== idMovie
                        && show.getFormatID()== formatId
                        && show.getTime().equals(date)
                        && show.getStartTime().equals(startTime) ){

                    bundle.putInt("showID",show.getShowID());
                    BookSeat bookSeat =  new BookSeat();
                    bookSeat.setArguments(bundle);
                    TestFragment testFragment = new TestFragment();
                    FragmentManager manager = getSupportFragmentManager();
                    manager.beginTransaction().replace(R.id.relativeLayout, bookSeat).commit();
                    manager.beginTransaction().replace(R.id.relativeLayout, testFragment).commit();
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
//        BookSeat testFragment = new BookSeat();
//        FragmentManager manager = getSupportFragmentManager();
//        manager.beginTransaction().replace(R.id.relativeLayout, testFragment).commit();
    }

    public Collection<ShowModel> getShowsByMovieAndFormatId(int movieId, int formatId){

        String url =localHost+"/api/Shows/GetShowsActiveByMovieId/"+movieId+"/"+formatId;
        try{
            String result =  new ReadJSONFeedTask().execute(url).get();
            JSONObject parentObject = new JSONObject(result);
            String notes = parentObject.getString("showsSelectedJson");
            Type collectionType = new TypeToken<Collection<ShowModel>>(){}.getType();
            Gson gson = new Gson();
            Collection<ShowModel> list = gson.fromJson(notes, collectionType);
            return list;
        }
        catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }



    public void getJsonMoviesActiveNowShowing(final VolleyCallback callback, final String datetime){
        final String url =localHost+"/api/Movies/GetMoviesActiveNowShow";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("nowStartTime", datetime);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(BookTicket.this).add(postRequest);
        }

    private class ReadJSONFeedTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return readJSONFeed(urls[0]);
        }

        protected void onPostExecute(String result) {

        }
        public String readJSONFeed(String address) {
            URL url = null;
            try {
                url = new URL(address);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            ;
            StringBuilder stringBuilder = new StringBuilder();
            HttpURLConnection urlConnection = null;
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                InputStream content = new BufferedInputStream(
                        urlConnection.getInputStream());
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return stringBuilder.toString();
        }
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }

    public class MovieModel {
        int MovieID;
        String MovieName;
        String ImageUrl;
        int Length;
        public List<GenreModel> Genres;
        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public String getMovieName() {
            return MovieName;
        }

        public void setMovieName(String movieName) {
            MovieName = movieName;
        }

        public String getImageUrl() {
            return ImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            ImageUrl = imageUrl;
        }

        public int getLength() {
            return Length;
        }

        public void setLength(int length) {
            Length = length;
        }

        public List<GenreModel> getGenres() {
            return Genres;
        }

        public void setGenres(List<GenreModel> genres) {
            Genres = genres;
        }



    }

    public class MovieViewModel{
        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public String getMovieName() {
            return MovieName;
        }

        public void setMovieName(String movieName) {
            MovieName = movieName;
        }

        int MovieID;
        String MovieName;
        List<FormatModel> Formats;

        public List<FormatModel> getFormats() {
            return Formats;
        }

        public void setFormats(List<FormatModel> formats) {
            Formats = formats;
        }
    }

    public class FormatModel{
        public int getFormatID() {
            return FormatID;
        }

        public void setFormatID(int formatID) {
            FormatID = formatID;
        }

        int FormatID;
        String FormatName;

        public String getFormatName() {
            return FormatName;
        }

        public void setFormatName(String formatName) {
            FormatName = formatName;
        }
    }

    public class GenreModel {
        public int GenreID ;
        public String GenreName ;
        public Boolean Active ;

        public int getGenreID() {
            return GenreID;
        }

        public void setGenreID(int genreID) {
            GenreID = genreID;
        }

        public String getGenreName() {
            return GenreName;
        }

        public void setGenreName(String genreName) {
            GenreName = genreName;
        }

        public Boolean getActive() {
            return Active;
        }

        public void setActive(Boolean active) {
            Active = active;
        }


    }

    public class ShowModel{

        int ShowID ;

        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public int getFormatID() {
            return FormatID;
        }

        public void setFormatID(int formatID) {
            FormatID = formatID;
        }

        int MovieID ;
        int FormatID;
        String Time ;
        String StartTime;
        String EndTime ;
        boolean Active ;

        public int getShowID() {
            return ShowID;
        }

        public void setShowID(int showID) {
            ShowID = showID;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String startTime) {
            StartTime = startTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public boolean isActive() {
            return Active;
        }

        public void setActive(boolean active) {
            Active = active;
        }
    }

    public class ShowViewModel {
         int ShowID;
         String RoomName;
         String Time;
         String StartTime;
         String EndTime ;
         List<TicketModel> Tickets;

        public int getShowID() {
            return ShowID;
        }

        public void setShowID(int showID) {
            ShowID = showID;
        }

        public String getRoomName() {
            return RoomName;
        }

        public void setRoomName(String roomName) {
            RoomName = roomName;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String startTime) {
            StartTime = startTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public Collection<TicketModel> getTickets() {
            return Tickets;
        }

        public void setTickets(List<TicketModel> tickets) {
            Tickets = tickets;
        }
    }

    public class TicketModel {
        public int getSeatID() {
            return SeatID;
        }

        public void setSeatID(int seatID) {
            SeatID = seatID;
        }

        public String getTicketCode() {
            return TicketCode;
        }

        public void setTicketCode(String ticketCode) {
            TicketCode = ticketCode;
        }

        public Double getPrice() {
            return Price;
        }

        public void setPrice(Double price) {
            Price = price;
        }

        public String getSeatName() {
            return SeatName;
        }

        public void setSeatName(String seatName) {
            SeatName = seatName;
        }

         int SeatID;
         String TicketCode;
         Double Price;
         String SeatName ;
    }


}
