package com.example.admin.cinemaproject;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Admin on 7/13/2017.
 */

public class Movie {

        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public String getMovieName() {
            return MovieName;
        }

        public void setMovieName(String movieName) {
            MovieName = movieName;
        }

        public String getImageUrl() {
            return ImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            ImageUrl = imageUrl;
        }

        public int getLength() {
            return Length;
        }

        public void setLength(int length) {
            Length = length;
        }

        public String getOpeningDate() {
            return OpeningDate;
        }

        public void setOpeningDate(String openingDate) {
            OpeningDate = openingDate;
        }

        public Collection<FormatModel> getFormats() {
            return Formats;
        }

        public void setGenres(Collection<FormatModel> formats) {
            Formats = formats;
        }

        int MovieID ;
        String MovieName ;
        String ImageUrl ;
        int Length ;
        String OpeningDate ;
        Collection<FormatModel> Formats ;

    public class FormatModel
    {
        public int getFormatID() {
            return FormatID;
        }

        public void setFormatID(int formatID) {
            FormatID = formatID;
        }

        public String getFormatName() {
            return FormatName;
        }

        public void setFormatName(String formatName) {
            FormatName = formatName;
        }

        int FormatID ;
         String FormatName ;
    }


}
