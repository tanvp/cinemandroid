package com.example.admin.cinemaproject;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Admin on 7/16/2017.
 */

public class TransactionAdapter extends ArrayAdapter<FmTransactionHistory.TransactionModel> {

    public TransactionAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<FmTransactionHistory.TransactionModel> objects) {
        super(context, resource, objects);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        //View v = View.inflate(mContext, R.layout.fragment_fm_book_seat_row, null);
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.fragment_fm_transaction_history_row, null);
        }
        FmTransactionHistory.TransactionModel t = getItem(i);
        if (t != null) {
            TextView txtDate = (TextView) view.findViewById(R.id.txtDateTime);
            TextView txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            TextView txtOrderCode = (TextView) view.findViewById(R.id.txtCode);
            txtDate.setText(t.getCreateDate().toString());
            txtPrice.setText(t.getTotalAmount().toString());
            txtOrderCode.setText(t.getOrderId());
        }
        return view;
    }


    }

