package com.example.admin.cinemaproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]*$";
    Pattern pattern;
    Matcher matcher;
    EditText txtUsername, txtPassword, txtConfirm, txtAddress, txtEmail, txtPhone;
    String username, password, confirm, address, email, phone;
    Button button;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtUsername = (EditText) view.findViewById(R.id.txtUsername);
        txtPassword = (EditText) view.findViewById(R.id.txtPassword);
        txtConfirm = (EditText) view.findViewById(R.id.txtConfirm);
        txtAddress = (EditText) view.findViewById(R.id.txtAddress);
        txtEmail = (EditText) view.findViewById(R.id.txtEmail);
        txtPhone = (EditText) view.findViewById(R.id.txtPhone);
        button = (Button) view.findViewById(R.id.btnRegister);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateInput()) {
                    CheckRegister(username, password, address, email, phone);
                }
            }
        });
    }

    private boolean validateInput() {
        boolean valid = true;

        username = txtUsername.getText().toString().trim();
        password = txtPassword.getText().toString().trim();
        confirm = txtConfirm.getText().toString().trim();
        address = txtAddress.getText().toString().trim();
        email = txtEmail.getText().toString().trim();
        phone = txtPhone.getText().toString().trim();

        if (username.isEmpty() || password.isEmpty() || confirm.isEmpty()
                || address.isEmpty() || email.isEmpty() || phone.isEmpty()) {
            valid = false;
            Toast.makeText(getActivity().getBaseContext(), "Vui lòng điền toàn bộ thông tin", Toast.LENGTH_SHORT).show();
        }
        else if (!validateUsername(username)) {
            valid = false;
            Toast.makeText(getActivity().getBaseContext(), "Bí danh chỉ gồm chữ cái và số.", Toast.LENGTH_SHORT).show();
        }
        else if (username.length() < 6 || password.length() < 6) {
            valid = false;
            Toast.makeText(getActivity().getBaseContext(), "Bí danh và mật khẩu phải chứa nhiều hơn 6 ký tự.", Toast.LENGTH_LONG).show();
        }
        else if (!password.equals(confirm)) {
            valid = false;
            Toast.makeText(getActivity().getBaseContext(), "Mật khẩu nhập lại không trùng khớp.", Toast.LENGTH_SHORT).show();
        }
        else if (!validateEmail(email)) {
            valid = false;
            Toast.makeText(getActivity().getBaseContext(), "Sai định dạng email.", Toast.LENGTH_SHORT).show();
        }

        return  valid;
    }

    private boolean validateUsername(String username) {
        pattern = Pattern.compile(USERNAME_PATTERN);
        matcher = pattern.matcher(username);
        return matcher.matches();
    }

    private boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    };
    public void CheckRegister(final String username, final String password, final String address, final String email, final String phone){
        final String url =Json.localHost+"/api/Accounts/checkUserSignUp";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject json = new JSONObject(response);
                            Boolean checkResult = json.getBoolean("isSuccess");
                            String messResult = json.getString("message");
                            if(checkResult){
                                LoginFragment loginFragment = new LoginFragment();
                                FragmentManager manager = getFragmentManager();
                                manager.beginTransaction().replace(R.id.mainLayout, loginFragment).addToBackStack("tag").commit();
                                Toast.makeText(getContext(), "Register successfully", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(getContext(),messResult,Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("Username", username);
                params2.put("Password", password);
                params2.put("Address", address);
                params2.put("Email", email);
                params2.put("Phone", phone);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }

}
