package com.example.admin.cinemaproject;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {
    ListView listView;
    Context context;

    public MovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false) ;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context = getActivity();
        listView = (ListView) view.findViewById(R.id.listView);
        Date today = new Date();
        today.setHours(0);
        getMovies2();
    }
    public void getMovies2(){
        final String url =Json.localHost+"/api/Movies";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject parentObject = new JSONObject(response);
                            String notes = parentObject.getString("movieJson");
                            Type collectionType = new TypeToken<Collection<Movie>>(){}.getType();
                            Gson gson = new Gson();
                            Collection<Movie> list = gson.fromJson(notes, collectionType);
                            ArrayList<Movie> listMovie = new ArrayList<>();
                            for (Movie movie:
                                 list) {
                                listMovie.add(movie);
                            }
                            MovieListAdapter adapter = new MovieListAdapter(context, R.layout.activity_movie_list_item, listMovie);
                            listView.setAdapter(adapter);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringRequest);
    }

}
