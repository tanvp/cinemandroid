package com.example.admin.cinemaproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 7/13/2017.
 */

public class MovieListAdapter extends ArrayAdapter<Movie> {

    public MovieListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Movie> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.activity_movie_list_item, null);
        }
        Movie m = getItem(position);

        if (m != null) {
            TextView txtID = (TextView) view.findViewById(R.id.txtID);
            TextView txtName = (TextView) view.findViewById(R.id.txtName);
            ImageView ivUrl = (ImageView) view.findViewById(R.id.ivImage);
            TextView txtLength = (TextView) view.findViewById(R.id.txtLength);
            TextView txtFormats = (TextView) view.findViewById(R.id.txtFormats);
            TextView txtReleaseDate = (TextView) view.findViewById(R.id.txtReleaseDate);
            Date today = new Date();
            today.setHours(0);
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Bitmap bitmap;
            txtName.setText(m.getMovieName());
            txtLength.setText(m.getLength() + " phút");
            txtReleaseDate.setText(formatter.format(today));
            String formatString="";
            for (Movie.FormatModel format:
                 m.getFormats()) {
                formatString+=format.getFormatName();
                formatString+="     ";
            }
            txtFormats.setText(formatString);
            Picasso.with(getContext()).load(m.getImageUrl()).into(ivUrl);
        }
        return view;
    }
}
