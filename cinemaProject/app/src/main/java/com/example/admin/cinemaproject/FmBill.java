package com.example.admin.cinemaproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FmBill extends Fragment {
    private ListView lvOrder;
    private BillAdapter adapter;
    private List<BookSeat.TicketModel> listTicketSelected;
    public View view;
    UserLocalStore userLocalStore;
    double sum;
    double wallet;

    public FmBill() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_fm_bill, container, false);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       try{
           userLocalStore = new UserLocalStore(getContext());
           getUserwallet();
           getUserwallet();
           String json = getArguments().getString("LIST");
           Type collectionType = new TypeToken<Collection<BookSeat.TicketModel>>(){}.getType();
           Gson gson = new Gson();
           Collection<BookSeat.TicketModel> list = gson.fromJson(json, collectionType);

           json = getArguments().getString("SHOWSELECTED");
           final BookSeat.ShowViewModel showSelected = gson.fromJson(json, BookSeat.ShowViewModel.class);

           TextView txtRoom = (TextView) view.findViewById(R.id.txtRoom);
           TextView txtMovieName = (TextView) view.findViewById(R.id.txtMovieName);
           TextView txtStart = (TextView) view.findViewById(R.id.txtStart);
           TextView txtEnd = (TextView) view.findViewById(R.id.txtEnd);
           TextView txtDate = (TextView) view.findViewById(R.id.txtEnd);
           txtMovieName.setText(showSelected.getMovieAndFormatName());
           txtStart.setText(showSelected.getStartTime());
           txtEnd.setText(showSelected.getEndTime());
           txtRoom.setText(showSelected.getRoomName());

           lvOrder = (ListView) view.findViewById(R.id.lvOrder);
           sum =0;
           listTicketSelected = new ArrayList<>();
           for (BookSeat.TicketModel ticket:
                list) {
               listTicketSelected.add(ticket);
               sum+= ticket.getPrice();
           }
           final TextView txtSum = (TextView) view.findViewById(R.id.txtSumOrder);
           txtSum.setText(String.valueOf(sum+" VND"));
           final TextView txtWallet = (TextView) view.findViewById(R.id.txtWallet);
           adapter = new BillAdapter(getActivity(), R.layout.fragment_fm_bill_row, listTicketSelected);
           lvOrder.setAdapter(adapter);
           Button btnBill = (Button) view.findViewById(R.id.btnBill);
           btnBill.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   if(wallet<sum){
                       Toast.makeText(getContext(), "Your wallet is not enough", Toast.LENGTH_SHORT).show();
                   }
                   else{

                       List<Integer> seatIdList = new ArrayList<Integer>();
                       for (BookSeat.TicketModel ticket:
                             listTicketSelected) {
                           seatIdList.add(ticket.getSeatID());
                       }
                       SendTransactionModel sendTransactionModel = new SendTransactionModel(userLocalStore.getLoggedInUser().getUsername(),
                                                                                            showSelected.ShowID,
                                                                                            seatIdList,
                                                                                            sum);
                       sendListTicketToServer(sendTransactionModel);
                   }
               }
           });
       } catch (Exception e){
           e.printStackTrace();
       }
    }
    public void sendListTicketToServer(final SendTransactionModel sendTransactionModel ){
        final String url =Json.localHost+"/api/Tickets/bookTicket";


        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject json = new JSONObject(response);
                            Boolean isSuccess = json.getBoolean("isSuccess");
                            if(isSuccess==true){
                                String orderCode = json.getString("generatedString");
                                //code quang ra man hinh moi
                                Bundle bundle = new Bundle();
                                bundle.putString("orderCode",orderCode);
                                FmOrderResult fmOrderResult = new FmOrderResult();
                                fmOrderResult.setArguments(bundle);
                                FragmentManager manager = getFragmentManager();
                                manager.beginTransaction().replace(R.id.mainLayout, fmOrderResult).commit();
                        }
                        else{
                                Toast.makeText(getContext(),"Can not order . Please contact admin for more information",Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                Gson gson = new Gson();
                String JsonString = gson.toJson(sendTransactionModel);
                params2.put("jsonResult",JsonString);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }

    public void getUserwallet(){
        final String url =Json.localHost+"/api/Accounts/getUserWalletPost";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject json = new JSONObject(response);
                            wallet = json.getDouble("userWallet");
                            TextView txtWallet = (TextView) view.findViewById(R.id.txtWallet);
                            txtWallet.setText(String.valueOf(wallet)+ "VND");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                UserLocalStore userLocalStore= new UserLocalStore(getContext());
                params2.put("username", userLocalStore.getLoggedInUser().getUsername());
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }

    public class TicketModel{
        int SeatID ;
        String TicketCode ;
        Double Price ;
        String SeatName ;

        public String getSeatTypeName() {
            return SeatTypeName;
        }

        public void setSeatTypeName(String seatTypeName) {
            SeatTypeName = seatTypeName;
        }

        String SeatTypeName ;
        public int getSeatID() {
            return SeatID;
        }

        public void setSeatID(int seatID) {
            SeatID = seatID;
        }

        public String getTicketCode() {
            return TicketCode;
        }

        public void setTicketCode(String ticketCode) {
            TicketCode = ticketCode;
        }

        public Double getPrice() {
            return Price;
        }

        public void setPrice(Double price) {
            Price = price;
        }

        public String getSeatName() {
            return SeatName;
        }

        public void setSeatName(String seatName) {
            SeatName = seatName;
        }
    }

    public class SendTransactionModel{
        String username;
        Integer showId;
        List<Integer>  seatIdList;
        Double totalPrice;

        public SendTransactionModel(String username, Integer showId, List<Integer> seatIdList, Double totalPrice) {
            this.username = username;
            this.showId = showId;
            this.seatIdList = seatIdList;
            this.totalPrice = totalPrice;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Integer getShowId() {
            return showId;
        }

        public void setShowId(Integer showId) {
            this.showId = showId;
        }

        public List<Integer> getSeatIdList() {
            return seatIdList;
        }

        public void setSeatIdList(List<Integer> seatIdList) {
            this.seatIdList = seatIdList;
        }

        public Double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Double totalPrice) {
            this.totalPrice = totalPrice;
        }
    }
}
