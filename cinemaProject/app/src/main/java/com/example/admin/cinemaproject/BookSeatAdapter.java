package com.example.admin.cinemaproject;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by PhungTan on 7/12/2017.
 */

public class BookSeatAdapter extends ArrayAdapter<BookSeat.TicketModel> {

    private List<BookSeat.TicketModel> mShowList;
    public ArrayList<InfoRowdata> infodata;
    public ArrayList<BookSeat.TicketModel> idSeatLíst;
    public double sum = 0 ;
    public BookSeatAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<BookSeat.TicketModel> objects) {
        super(context, resource, objects);
        mShowList=objects;
        infodata = new ArrayList<InfoRowdata>();
        for (int i = 0; i < objects.size(); i++) {
            infodata.add(new InfoRowdata(false, i));
            // System.out.println(i);
            //System.out.println("Data is == "+data[i]);
        }

    }

    /*public BookSeatAdapter(Context mContext, List<BookSeat.TicketModel> mShowList) {
        this.mContext = mContext;
        this.mShowList = mShowList;
    }*/

    /*@Override
    public int getCount() {
        return mShowList.size();
    }

    @Override
    public BookSeat.TicketModel getItem(int i) {
        return mShowList.get(i);
    }*/

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i,  View view, ViewGroup viewGroup) {
        //View v = View.inflate(mContext, R.layout.fragment_fm_book_seat_row, null);
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.fragment_fm_book_seat_row, null);
        }

        BookSeat.TicketModel t = getItem(i);

        if (t != null) {
            TextView txtSeatName = (TextView) view.findViewById(R.id.txtSeatName);
            TextView txtSeatType = (TextView) view.findViewById(R.id.txtSeatType);
            TextView txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            txtSeatName.setText("Name: " + t.getSeatName());
            txtSeatType.setText("Type: " + t.getSeatTypeName());
            txtPrice.setText("Price: " + t.getPrice().toString());
            view.setTag(t.getSeatID());
            final CheckBox cb = (CheckBox) view
                    .findViewById(R.id.checkBox);
            cb.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (infodata.get(i).isclicked) {
                        infodata.get(i).isclicked = false;
                    } else {
                        infodata.get(i).isclicked = true;
                    }
                    idSeatLíst= new ArrayList<>();
                    sum =0;
                    for (int i = 0; i < infodata.size(); i++) {
                        if (infodata.get(i).isclicked) {
                            System.out.println("Selectes Are == " + v.getTag());
                            idSeatLíst.add(mShowList.get(i));
                          // sum+= mShowList.get(i).getPrice();
                        }
                    }
                 //   txtSum.setText(String.valueOf(sum));
                }
            });
            if (infodata.get(i).isclicked) {
                cb.setChecked(true);
            } else {
                cb.setChecked(false);
            }
        }
        return view;
    }
    public class InfoRowdata {

        public boolean isclicked=false;
        public int index;
    /*public String fanId;
    public String strAmount;*/

        public InfoRowdata(boolean isclicked,int index/*,String fanId,String strAmount*/)
        {
            this.index=index;
            this.isclicked=isclicked;
        /*this.fanId=fanId;
        this.strAmount=strAmount;*/
        }

    }}
