package com.example.admin.cinemaproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.cinemaproject.R.id.lvOrder;


/**
 * A simple {@link Fragment} subclass.
 */
public class FmTransactionHistory extends Fragment {
    private TransactionAdapter adapter;
    private List<TransactionModel> transactionModelList;
    UserLocalStore userLocalStore;
    ListView lvTrans;
    View view;
    public FmTransactionHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =inflater.inflate(R.layout.fragment_fm_transaction_history, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userLocalStore = new UserLocalStore(getContext());
        getTransactionHistory();
        lvTrans = (ListView) view.findViewById(R.id.lvTransaction);

    }

    public void getTransactionHistory(){
        final String url =Json.localHost+"/api/BalanceTransactions/GetTransactionHistory";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       try{
                           JSONObject json = new JSONObject(response);
                           Boolean isSuccess = json.getBoolean("isSuccess");
                            if(isSuccess){
                                JSONObject parentObject = new JSONObject(response);
                                String notes = parentObject.getString("listTransactionJson");
                                Type collectionType = new TypeToken<Collection<TransactionModel>>(){}.getType();
                                Gson gson = new Gson();
                                List<TransactionModel> list = gson.fromJson(notes, collectionType);
                                adapter = new TransactionAdapter(getActivity(), R.layout.fragment_fm_transaction_history_row, list);
                                lvTrans.setAdapter(adapter);
                            }
                           else{
                                Toast.makeText(getContext(),"Can not load transaction", Toast.LENGTH_SHORT).show();
                            }

                       }
                       catch (Exception e){

                       }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                Gson gson = new Gson();
                params2.put("username",userLocalStore.getLoggedInUser().getUsername());
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }

    public class TransactionModel{
        String CreateDate ;
        String OrderId;
        Double TotalAmount;

        public String getCreateDate() {
            return CreateDate;
        }

        public void setCreateDate(String createDate) {
            CreateDate = createDate;
        }

        public String getOrderId() {
            return OrderId;
        }

        public void setOrderId(String orderId) {
            OrderId = orderId;
        }

        public Double getTotalAmount() {
            return TotalAmount;
        }

        public void setTotalAmount(Double totalAmount) {
            TotalAmount = totalAmount;
        }
    }
}
