package com.example.admin.cinemaproject;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by PhungTan on 7/12/2017.
 */

public class BillAdapter extends ArrayAdapter<BookSeat.TicketModel> {

    private List<BookSeat.TicketModel> mShowList;
    public double sum = 0 ;
    public BillAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<BookSeat.TicketModel> objects) {
        super(context, resource, objects);
        mShowList=objects;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i,  View view, ViewGroup viewGroup) {
        //View v = View.inflate(mContext, R.layout.fragment_fm_book_seat_row, null);
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.fragment_fm_bill_row, null);
        }
        BookSeat.TicketModel t = getItem(i);
        if (t != null) {
            TextView txtSeatName = (TextView) view.findViewById(R.id.txtSeatName);
            TextView txtSeatType = (TextView) view.findViewById(R.id.txtSeatType);
            TextView txtPrice = (TextView) view.findViewById(R.id.txtPrice);
            txtSeatName.setText("Name: " + t.getSeatName());
            txtSeatType.setText("Type: " + t.getSeatTypeName());
            txtPrice.setText("Price: " + t.getPrice().toString());


            // TODO Auto-generated method stub
        }
        return view;
    }
public class InfoRowdata {

    public boolean isclicked=false;
    public int index;
    /*public String fanId;
    public String strAmount;*/

    public InfoRowdata(boolean isclicked,int index/*,String fanId,String strAmount*/)
    {
        this.index=index;
        this.isclicked=isclicked;
        /*this.fanId=fanId;
        this.strAmount=strAmount;*/
    }

}}
