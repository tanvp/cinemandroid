package com.example.admin.cinemaproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookSeat extends Fragment {
    private ListView lvTicket;
    private BookSeatAdapter adapter;
    private List<BookSeat.TicketModel> mTicketList;
    public List<BookSeat.TicketModel> listTicketSelected;
    public View view;
    List<Integer> idSelectedList;
    public BookSeat() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_book_seat, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Integer showID = getArguments().getInt("showID");
        lvTicket = (ListView) view.findViewById(R.id.lvTickets);
        ShowViewModel show = new ShowViewModel();
        Collection<ShowViewModel> showList = GetShowTicketsByShowId(showID);
        for (ShowViewModel item:
                showList) {
            show = item;
        }
        final Bundle bundle = new Bundle();
        Gson gson = new Gson();
        String json = gson.toJson(show);
        bundle.putString("SHOWSELECTED", json);
        mTicketList = new ArrayList<>();
        for (TicketModel ticket:
                show.Tickets) {
            mTicketList.add(ticket);
        }
       // Toast.makeText(getActivity().getBaseContext(), "Seat Fragment", Toast.LENGTH_SHORT).show();
        TextView txtMovieName = (TextView) view.findViewById(R.id.txtMovieName);
        TextView txtRoom = (TextView) view.findViewById(R.id.txtRoom);
        txtMovieName.setText(show.getMovieAndFormatName());
        txtRoom.setText(show.getRoomName());

            adapter = new BookSeatAdapter(getActivity(), R.layout.fragment_fm_book_seat_row, mTicketList);
            lvTicket.setAdapter(adapter);
            idSelectedList= new ArrayList<>();

            /*Toast.makeText(getActivity(), "set adapter duoc roi ", Toast.LENGTH_SHORT).show();*/

            Button btnBook = (Button) view.findViewById(R.id.btnBookSeat);

            btnBook.setOnClickListener(new View.OnClickListener() {
        //        StringBuilder s = new StringBuilder();
                @Override
                public void onClick(View v) {
                    listTicketSelected = new ArrayList<TicketModel>();
                    if(adapter.idSeatLíst!=null && adapter.idSeatLíst.size()>0){
                        for (BookSeat.TicketModel ticket:
                             adapter.idSeatLíst) {
                            Toast.makeText(getContext(), ticket.getSeatID()+"-", Toast.LENGTH_SHORT).show();
                            listTicketSelected.add(ticket);
                        }
                        Gson gson = new Gson();
                        String json = gson.toJson(listTicketSelected);
                        bundle.putString("LIST", json);
                        FmBill fmBill = new FmBill();
                        fmBill.setArguments(bundle);
                        FragmentManager manager = getFragmentManager();
                        manager.beginTransaction().replace(R.id.mainLayout, fmBill).addToBackStack("tag").commit();

//                        Gson gson = new Gson();
//                        String json = gson.toJson(mTicketList);
                      //  sendJsonTicketToServer(json);
                    }
                }
            });
    }
    public void sendJsonTicketToServer(final String jsonSend){
        final String url =Json.localHost+"/api/Movies/GetMoviesActiveNowShow";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       try{
                           JSONObject json = new JSONObject(response);
                           JSONObject json_LL = json.getJSONObject("LL");
                           String OrderTicket=json_LL.getString("OrderTicket");
                       }
                       catch (Exception e){

                       }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("jsonMessage", jsonSend);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }


    public Collection<ShowViewModel> GetShowTicketsByShowId(int showId){
        String url =Json.localHost+"/api/Shows/GetShowTicketsByShowId/"+showId;
        try{
            String result =  new Json.ReadJSONFeedTask().execute(url).get();
            JSONObject parentObject = new JSONObject(result);
            String notes = parentObject.getString("showTicketsJson");
            Type collectionType = new TypeToken<Collection<ShowViewModel>>(){}.getType();
            Gson gson = new Gson();
            Collection<ShowViewModel> list = gson.fromJson(notes, collectionType);
            return list;
        }
        catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }


    public class ShowViewModel {
        public int getShowID() {
            return ShowID;
        }

        public void setShowID(int showID) {
            ShowID = showID;
        }

        public String getRoomName() {
            return RoomName;
        }

        public void setRoomName(String roomName) {
            RoomName = roomName;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String startTime) {
            StartTime = startTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public List<TicketModel> getTickets() {
            return Tickets;
        }

        public void setTickets(List<TicketModel> tickets) {
            Tickets = tickets;
        }

        int ShowID ;
        String RoomName;
        String Time ;
        String StartTime;
        String EndTime ;
        String MovieAndFormatName;

        public String getMovieAndFormatName() {
            return MovieAndFormatName;
        }

        public void setMovieAndFormatName(String movieAndFormatName) {
            MovieAndFormatName = movieAndFormatName;
        }

        List<TicketModel> Tickets ;
    }

    public class TicketModel{
        int SeatID ;
        String TicketCode ;
        Double Price ;
        String SeatName ;

        public String getSeatTypeName() {
            return SeatTypeName;
        }

        public void setSeatTypeName(String seatTypeName) {
            SeatTypeName = seatTypeName;
        }

        String SeatTypeName ;
        public int getSeatID() {
            return SeatID;
        }

        public void setSeatID(int seatID) {
            SeatID = seatID;
        }

        public String getTicketCode() {
            return TicketCode;
        }

        public void setTicketCode(String ticketCode) {
            TicketCode = ticketCode;
        }

        public Double getPrice() {
            return Price;
        }

        public void setPrice(Double price) {
            Price = price;
        }

        public String getSeatName() {
            return SeatName;
        }

        public void setSeatName(String seatName) {
            SeatName = seatName;
        }
    }



}

