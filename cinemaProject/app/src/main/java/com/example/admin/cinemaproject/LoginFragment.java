package com.example.admin.cinemaproject;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    Button button;
    TextView txtRegister;
    EditText txtUsername, txtPassword;
    String username, password;
    UserLocalStore userLocalStore;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userLocalStore = new UserLocalStore(getContext());
        txtUsername = (EditText) view.findViewById(R.id.txtUsername);
        txtPassword = (EditText) view.findViewById(R.id.txtPassword);
        txtRegister = (TextView) view.findViewById(R.id.txtRegister);
        button = (Button) view.findViewById(R.id.btnSignIn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateInput()) {
                    Toast.makeText(getActivity().getBaseContext(), "Vui lòng điền bí danh và mật khẩu.", Toast.LENGTH_SHORT).show();
                }
                else {
                    CheckLogin(username, password);


                }
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.mainLayout, registerFragment).addToBackStack("tag").commit();
            }
        });
    }

    public boolean validateInput() {
        boolean valid = true;
        username = txtUsername.getText().toString();
        password = txtPassword.getText().toString();

        if (username.isEmpty() || password.isEmpty() ) {
            valid = false;
        }
        return valid;
    }

    public void CheckLogin(final String username, final String password){
        final String url =Json.localHost+"/api/Accounts/CheckUserLogin";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject json = new JSONObject(response);
                            Boolean checkResult = json.getBoolean("isSuccess");
                            if(checkResult){
                                User user = new User(username,password);
                                userLocalStore.storedUserData(user);
                                userLocalStore.setUserLoggedIn(true);
                                //Chỗ này là check thành công rồi nè Giang.
                                //Giang kích hoạt cái nav ở đây nha
                              //  Activity parent = getActivity();
                                ((MainActivity)getActivity()).setupMenu();//hình như cái này phải hem no idea, thu xem
                                //kt bien kia chua?? set dc k? luc kt value co dung k?

                                //mấy dòng dưới là t điều hướng qua trang phim của Giang á
                                MovieFragment movieFragment = new MovieFragment();
                                FragmentManager manager = getFragmentManager();
                                manager.beginTransaction().replace(R.id.mainLayout, movieFragment).addToBackStack("tag").commit();
                            }
                            else{
                                userLocalStore.setUserLoggedIn(false);
                                Toast.makeText(getContext(),"Can not sign in",Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("Username", username);
                params2.put("PassWord", password);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }

}
