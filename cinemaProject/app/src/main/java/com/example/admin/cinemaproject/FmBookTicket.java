package com.example.admin.cinemaproject;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONException;
import org.json.JSONObject;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



/**
 *
 *
 * A simple {@link Fragment} subclass.
 */
public class FmBookTicket extends Fragment {
    ArrayList<String> arraySpinner;
    HashMap<Integer,Integer> spinnerMapMovie = new HashMap<Integer, Integer>();
    HashMap<Integer,Integer> spinnerMapFormat = new HashMap<Integer, Integer>();
    public  String result="";

    public Collection<ShowModel> listShow;
    View view;
    public FmBookTicket() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fm_book_ticket, container, false);
        return view;

    }

    @Override
    public void onViewCreated(final View viewCreate, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(viewCreate, savedInstanceState);
        Button btnBook = (Button) view.findViewById(R.id.btnBook);
        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    //Intent i = new Intent(getContext(), BookSeat.class);
                    Bundle bundle = new Bundle();
                    Spinner spMovie= (Spinner) view.findViewById(R.id.spMovie);
                    Spinner spFormat= (Spinner) view.findViewById(R.id.spFormat);
                    Spinner spStartTime = (Spinner) view.findViewById(R.id.spTime);
                    Spinner spDate = (Spinner) view.findViewById(R.id.spDate);
                    int idMovie = spinnerMapMovie.get(spMovie.getSelectedItemPosition());
                    int formatId = spinnerMapFormat.get(spFormat.getSelectedItemPosition());
                    String startTime = spStartTime.getSelectedItem().toString();
                    String date = spDate.getSelectedItem().toString();
                    for (ShowModel show: listShow ) {
                        if(show.getMovieID()== idMovie
                                && show.getFormatID()== formatId
                                && show.getTime().equals(date)
                                && show.getStartTime().equals(startTime) ){

                            bundle.putInt("showID",show.getShowID());
                            BookSeat bookSeat =  new BookSeat();
                            bookSeat.setArguments(bundle);
                            FragmentManager manager = getFragmentManager();
                            manager.beginTransaction().replace(R.id.mainLayout, bookSeat).addToBackStack("tag").commit();
                        }
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        getJsonMoviesActiveNowShowing(new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                try {
                    arraySpinner = new ArrayList<String>();
                    spinnerMapMovie = new HashMap<Integer, Integer>();
                    JSONObject parentObject = new JSONObject(result);
                    String notes = parentObject.getString("movieJson");
                    Type collectionType = new TypeToken<Collection<MovieViewModel>>() {}.getType();
                    Gson gson = new Gson();
                    final List<MovieViewModel> list = gson.fromJson(notes, collectionType);
                    for (int i = 0; i < list.size(); i++) {
                        spinnerMapMovie.put(i, list.get(i).getMovieID());
                        arraySpinner.add(list.get(i).getMovieName());
                    }
                    final Spinner spMovie = (Spinner) viewCreate.findViewById(R.id.spMovie);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arraySpinner);
                    spMovie.setAdapter(adapter);

                    spMovie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            final int idMovie = spinnerMapMovie.get(spMovie.getSelectedItemPosition());
                            final String movieName = spMovie.getSelectedItem().toString();
                            arraySpinner = new ArrayList<String>();
                            spinnerMapFormat = new HashMap<Integer, Integer>();
                            for (MovieViewModel movie:
                                    list) {
                                if(movie.getMovieID()== idMovie){
                                    for (int i = 0; i < movie.getFormats().size(); i++)
                                    {
                                        spinnerMapFormat.put(i ,movie.getFormats().get(i).getFormatID());
                                        arraySpinner.add(movie.getFormats().get(i).getFormatName());
                                    }
                                }
                            }
                            final Spinner spFormat = (Spinner) viewCreate.findViewById(R.id.spFormat);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arraySpinner);
                            spFormat.setAdapter(adapter);

                            spFormat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    int formatId = spinnerMapFormat.get(spFormat.getSelectedItemPosition());
                                    listShow=getShowsByMovieAndFormatId(idMovie, formatId);
                                    arraySpinner = new ArrayList<String>();
                                    Set<String> set =new HashSet<>();
                                    for ( ShowModel show: listShow
                                            ) {
                                        set.add(show.getTime());
                                    }
                                    if(set!=null){
                                        for ( String sh: set
                                                ) {
                                            arraySpinner.add(sh);
                                        }
                                        if(arraySpinner!=null) {
                                            Collections.sort(arraySpinner);
                                        }
                                        final Spinner spDate = (Spinner) viewCreate.findViewById(R.id.spDate);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arraySpinner);
                                        spDate.setAdapter(adapter);
                                        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                String txtDate = spDate.getSelectedItem().toString();
                                                arraySpinner = new ArrayList<String>();
                                                for (ShowModel show: listShow
                                                        ) {
                                                    if(show.getTime().equals(txtDate)){
                                                        arraySpinner.add(show.getStartTime());
                                                    }
                                                }
                                                final Spinner spTime = (Spinner) viewCreate.findViewById(R.id.spTime);
                                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arraySpinner);
                                                spTime.setAdapter(adapter);
                                            }
                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                            }
                                        });
                                    }
                                }



                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, "2017-05-26 00:00:00.000");

    }

    public void getMovies2(){
        final String url =Json.localHost+"/api/Movies/4";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject parentObject = new JSONObject(response);
                            String notes = parentObject.getString("movieJson");
                            Type collectionType = new TypeToken<Collection<BookTicket.MovieModel>>(){}.getType();
                            Gson gson = new Gson();
                            Collection<BookTicket.MovieModel> list = gson.fromJson(notes, collectionType);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        RequestQueue queue = Volley.newRequestQueue(getContext());
        queue.add(stringRequest);
    }

    public void bookTicketClicks(View v){

//        BookSeat testFragment = new BookSeat();
//        FragmentManager manager = getSupportFragmentManager();
//        manager.beginTransaction().replace(R.id.relativeLayout, testFragment).commit();
    }

    public Collection<ShowModel> getShowsByMovieAndFormatId(int movieId, int formatId){

        String url =Json.localHost+"/api/Shows/GetShowsActiveByMovieId/"+movieId+"/"+formatId;
        try{
            String result =  new Json.ReadJSONFeedTask().execute(url).get();
            JSONObject parentObject = new JSONObject(result);
            String notes = parentObject.getString("showsSelectedJson");
            Type collectionType = new TypeToken<Collection<ShowModel>>(){}.getType();
            Gson gson = new Gson();
            Collection<ShowModel> list = gson.fromJson(notes, collectionType);
            return list;
        }
        catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }

    public void getJsonMoviesActiveNowShowing(final VolleyCallback callback, final String datetime){
        final String url =Json.localHost+"/api/Movies/GetMoviesActiveNowShow";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("nowStartTime", datetime);
                return new JSONObject(params2).toString().getBytes();
            }
            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };
        Volley.newRequestQueue(getContext()).add(postRequest);
    }





    public interface VolleyCallback{
        void onSuccess(String result);
    }

    public class MovieModel {
        int MovieID;
        String MovieName;
        String ImageUrl;
        int Length;
        public List<GenreModel> Genres;
        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public String getMovieName() {
            return MovieName;
        }

        public void setMovieName(String movieName) {
            MovieName = movieName;
        }

        public String getImageUrl() {
            return ImageUrl;
        }

        public void setImageUrl(String imageUrl) {
            ImageUrl = imageUrl;
        }

        public int getLength() {
            return Length;
        }

        public void setLength(int length) {
            Length = length;
        }

        public List<GenreModel> getGenres() {
            return Genres;
        }

        public void setGenres(List<GenreModel> genres) {
            Genres = genres;
        }



    }

    public class MovieViewModel{
        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public String getMovieName() {
            return MovieName;
        }

        public void setMovieName(String movieName) {
            MovieName = movieName;
        }

        int MovieID;
        String MovieName;
        List<FormatModel> Formats;

        public List<FormatModel> getFormats() {
            return Formats;
        }

        public void setFormats(List<FormatModel> formats) {
            Formats = formats;
        }
    }

    public class FormatModel{
        public int getFormatID() {
            return FormatID;
        }

        public void setFormatID(int formatID) {
            FormatID = formatID;
        }

        int FormatID;
        String FormatName;

        public String getFormatName() {
            return FormatName;
        }

        public void setFormatName(String formatName) {
            FormatName = formatName;
        }
    }

    public class GenreModel {
        public int GenreID ;
        public String GenreName ;
        public Boolean Active ;

        public int getGenreID() {
            return GenreID;
        }

        public void setGenreID(int genreID) {
            GenreID = genreID;
        }

        public String getGenreName() {
            return GenreName;
        }

        public void setGenreName(String genreName) {
            GenreName = genreName;
        }

        public Boolean getActive() {
            return Active;
        }

        public void setActive(Boolean active) {
            Active = active;
        }


    }

    public class ShowModel{

        int ShowID ;

        public int getMovieID() {
            return MovieID;
        }

        public void setMovieID(int movieID) {
            MovieID = movieID;
        }

        public int getFormatID() {
            return FormatID;
        }

        public void setFormatID(int formatID) {
            FormatID = formatID;
        }

        int MovieID ;
        int FormatID;
        String Time ;
        String StartTime;
        String EndTime ;
        boolean Active ;

        public int getShowID() {
            return ShowID;
        }

        public void setShowID(int showID) {
            ShowID = showID;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String startTime) {
            StartTime = startTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public boolean isActive() {
            return Active;
        }

        public void setActive(boolean active) {
            Active = active;
        }
    }

    public class ShowViewModel {
        int ShowID;
        String RoomName;
        String Time;
        String StartTime;
        String EndTime ;
        List<TicketModel> Tickets;

        public int getShowID() {
            return ShowID;
        }

        public void setShowID(int showID) {
            ShowID = showID;
        }

        public String getRoomName() {
            return RoomName;
        }

        public void setRoomName(String roomName) {
            RoomName = roomName;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String startTime) {
            StartTime = startTime;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public Collection<TicketModel> getTickets() {
            return Tickets;
        }

        public void setTickets(List<TicketModel> tickets) {
            Tickets = tickets;
        }
    }

    public class TicketModel {
        public int getSeatID() {
            return SeatID;
        }

        public void setSeatID(int seatID) {
            SeatID = seatID;
        }

        public String getTicketCode() {
            return TicketCode;
        }

        public void setTicketCode(String ticketCode) {
            TicketCode = ticketCode;
        }

        public Double getPrice() {
            return Price;
        }

        public void setPrice(Double price) {
            Price = price;
        }

        public String getSeatName() {
            return SeatName;
        }

        public void setSeatName(String seatName) {
            SeatName = seatName;
        }

        int SeatID;
        String TicketCode;
        Double Price;
        String SeatName ;
    }


}
